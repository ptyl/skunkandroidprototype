package com.cloudon.libreoffice.android.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cloudon.libreoffice.android.DesktopController;
import com.sun.star.beans.PropertyState;
import com.sun.star.beans.PropertyValue;

public class Utils implements Glossary {

	public static File copy(File src, File dst) {
		try {

			return copy(new FileInputStream(src), dst);

		} catch (Throwable throwable) {

			throw up("copy \"" + src + "\" to \"" + dst + "\"", throwable);
		}
	}

	public static File copy(InputStream in, File dst) {
		try {
			log("Copying to " + dst);

			OutputStream out = new FileOutputStream(dst);

			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();

			log("completed copying to " + dst);

			return dst;

		} catch (Throwable throwable) {

			throw up("copy to \"" + dst + "\"", throwable);
		}
	}

	public static File createTempCopy(int srcResId, Activity activity) {
		return copy(activity.getResources().openRawResource(srcResId),
				createTempFile(srcResId, activity));
	}

	private static File createTempFile(int srcResId, Activity activity) {
		String name = "temp_"
				+ activity.getResources().getResourceName(srcResId)
						.replace(".", "_").replace("/", "_").replace(":", "_");

		try {

			return File.createTempFile(name,
					"."
							+ DesktopController.INSTANCE.extension.get()
									.getExtension(), activity.getCacheDir());

		} catch (Throwable throwable) {

			throw Utils.up("Creating a temp file for resoruce ID " + srcResId,
					throwable);
		}
	}

	public static RuntimeException up(String context, Throwable throwable) {
		context = context + " failed due to "
				+ throwable.getClass().getCanonicalName() + ", Reason: "
				+ throwable.getMessage();
		Log.e(ERROR_TAG, context, throwable);

		return new RuntimeException(context, throwable);
	}

	public static RuntimeException up(String context) {
		return up(context, new Throwable());
	}

	public static void log(String... strings) {
		for (int i = 0; i < strings.length; i++) {
			Log.d(NOTE_TAG, strings[i]);
		}
	}

	public static void err(String... strings) {
		for (int i = 0; i < strings.length; i++) {
			Log.e(ERROR_TAG, strings[i]);
		}
	}

	public static void sleepSeconds(long seconds) {
		sleepMillis(seconds * 1000L);
	}

	public static void sleepMillis(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {

		}
	}

	public static void setButton(Activity activity, int buttonRefId,
			final Runnable runnable) {
		((Button) activity.findViewById(buttonRefId))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						runnable.run();

					}
				});
	}

	public static void run(Runnable runnable) {
		new Thread(runnable).start();
	}

	public static void toast(String message, Activity activity) {
		Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
	}

	public static <T> T notNull(T t, String message) {
		if (t == null) {
			throw new RuntimeException(message + ". Got null");
		}
		return t;
	}

	public static void confirmOrStubDialog(Context context, String question,
			final Runnable onYes) {

		confirmDialog(context, question, "Yes", "Cancel", onYes,
				new Runnable() {

					@Override
					public void run() {

					}
				});
	}

	public static void confirmDialog(Context context, String question,
			final Runnable onYes, final Runnable onNo) {

		confirmDialog(context, question, "Yes", "No", onYes, onNo);
	}

	public static void confirmDialog(Context context, String question,
			String yesText, String noText, final Runnable onYes,
			final Runnable onNo) {

		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					onYes.run();
					break;

				case DialogInterface.BUTTON_NEGATIVE:

					break;
				}
			}
		};

		new AlertDialog.Builder(context).setMessage(question + "?")
				.setPositiveButton(yesText, listener)
				.setNegativeButton(noText, listener).show();

	}

	public static String toUrl(File file) {

		return "file://" + file;
	}

	public static PropertyValue toProp(String name, Object value) {
		return toProp(name, value, null, null);
	}

	public static PropertyValue toProp(String name, Object value,
			Integer handle, PropertyState state) {
		PropertyValue prop = new PropertyValue();
		prop.Name = name;
		prop.Value = value;
		if (handle != null) {
			prop.Handle = handle;
		}
		if (state != null) {
			prop.State = state;
		}
		return prop;
	}

	public static <T> T[] toArray(T... ts) {
		return ts;
	}
}
