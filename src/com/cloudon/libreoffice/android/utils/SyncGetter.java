package com.cloudon.libreoffice.android.utils;

public abstract class SyncGetter<T> extends Utils {
	private final Object lock = new Object();
	private final Class<T> classOrAllowNull;

	private T value = null;

	public SyncGetter(Class<T> classOrAllowNull) {
		this.classOrAllowNull = classOrAllowNull;
	}

	public SyncGetter<T> clear() {
		value = null;

		return this;
	}

	public T get() {
		if (value == null) {
			synchronized (lock) {
				if (value == null) {

					createImpl();
				}
			}
		}

		return value;
	}

	protected abstract T create() throws Throwable;

	private void createImpl() {
		try {

			value = create();

		} catch (Throwable throwable) {

			throw up(getErrorMessage(), throwable);
		}

		evalCreatedValue();
	}

	private void evalCreatedValue() {
		if (value == null) {

			if (isPreventNull()) {

				throw up("Failed to create instance of type \""
						+ classOrAllowNull.getCanonicalName()
						+ "\". created null instead");
			} else {

				err("Null created instead of instance");
			}
		} else {
			log("created an instance of type:", value.getClass()
					.getCanonicalName(), "Instance.toString()=" + value);
		}
	}

	private String getErrorMessage() {
		String message = "Failed to create instance";
		if (isPreventNull()) {
			message += " of type: \"" + classOrAllowNull.getCanonicalName()
					+ "\"";
		}

		return message;
	}

	private boolean isPreventNull() {
		return classOrAllowNull != null;
	}

}
