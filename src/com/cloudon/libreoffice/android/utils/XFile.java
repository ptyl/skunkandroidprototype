package com.cloudon.libreoffice.android.utils;

import java.io.File;

import com.cloudon.libreoffice.android.DesktopController;
import com.cloudon.libreoffice.android.UnoUtils;
import com.cloudon.libreoffice.android.enums.UnoUrlTarget;
import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.task.ErrorCodeIOException;

public class XFile extends Utils {

	public XFile(File file) {
		this(file, UnoUrlTarget.DEFAULT);
	}

	private XFile(File file, UnoUrlTarget target) {
		super();
		this.file = file;
		this.target = target;
	}

	public String getUrl() {
		return toUrl(file);
	}

	private static final PropertyValue[] NO_PROPS = new PropertyValue[0];

	private final File file;
	private final UnoUrlTarget target;
	private final SyncGetter<Object> object = new SyncGetter<Object>(
			Object.class) {

		@Override
		protected Object create() throws Throwable {

			String url = getUrl();

			log("Creating component using url =\"" + url + "\" for " + this);

			return uno().loader.get().loadComponentFromURL(url,
					target.getTarget(), 0, NO_PROPS);
		}

	};

	public final SyncGetter<XStorable> xStorable = new SyncGetter<XStorable>(
			XStorable.class) {

		@Override
		protected XStorable create() throws Throwable {

			return uno().queryInterface(XStorable.class, object.get());
		}
	};

	public final SyncGetter<XComponent> xComponent = new SyncGetter<XComponent>(
			XComponent.class) {

		@Override
		protected XComponent create() throws Throwable {

			return uno().queryInterface(XComponent.class, object.get());
		}
	};

	private UnoUtils uno() {
		return DesktopController.INSTANCE.uno();
	}

	@Override
	public String toString() {

		return target + "-" + file;
	}

	public void close() {

		try {
			log("disposing of the xComponent of " + this);

			xComponent.get().dispose();

			log("file should be closed. removing " + this + " from UNO utils");

			uno().remove(this);

		} catch (Throwable throwable) {
			throw up("failed to close " + this, throwable);
		}
	}

	public void save() {

		String url = getUrl();

		try {

			log("Saving " + this + " by calling", "storeToUrl(\"" + url
					+ "\",[])");

			xStorable.get().store();// storeToURL(getUrl(), NO_PROPS);

			log(this + " should now be saved");

		} catch (ErrorCodeIOException errorCodeIOException) {
			throw up("Failed to save " + this + ". ErrorCode: "
					+ errorCodeIOException.ErrCode, errorCodeIOException);
		} catch (Throwable throwable) {
			throw up("Failed to save " + this, throwable);
		}
	}

	public void open() {
		String url = getUrl();

		try {

			log("now trying to load " + this + " using:",
					"xCompoentLoader.loadComponentFromURL(\"" + url + "\", \""
							+ target.getTarget()
							+ "\", 0, new PropertyValue[0])");

			uno().loader.get().loadComponentFromURL(url, target.getTarget(), 0,
					NO_PROPS);

			log(this + " should now be loaded");

			closeOtherDocuments();

		} catch (Throwable throwable) {

			throw up("Failed to open " + this, throwable);

		}
	}

	private void closeOtherDocuments() {

		log("Closing all other documents, other than " + this);

		uno().closeOthers(this);

	}

	@Override
	public boolean equals(Object o) {
		if ((o == null) || !(o instanceof XFile)) {
			return false;
		}
		XFile other = (XFile) o;
		return file.equals(other.file) && target.equals(other.target);
	}

	@Override
	public int hashCode() {

		return 3 * file.hashCode() + 5 * target.hashCode();
	}

	public File getFile() {

		return file;
	}

}
