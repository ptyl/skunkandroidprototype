package com.cloudon.libreoffice.android.utils;

public interface Glossary {
	public static final String SOURCE_FILE_REF_ID_INT_KEY = "loFile",
			NL = "\r\n", LOG_TAG = "LO PROTO: ", NOTE_TAG = LOG_TAG + "NOTE",
			ERROR_TAG = LOG_TAG + "ERROR";

	public static final int BITMAP_CYCLER_BUFFER_SIZE = 20;
}
