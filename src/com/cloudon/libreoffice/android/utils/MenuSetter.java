package com.cloudon.libreoffice.android.utils;

import java.util.concurrent.atomic.AtomicReference;

import android.app.Activity;


public class MenuSetter<E extends Enum<?>> extends Utils {

	private final AtomicReference<E> reference;

	private MenuSetter(E defaultValue) {
		reference = new AtomicReference<E>(defaultValue);
	}

	public static <V extends Enum<?>> MenuSetter<V> create(V defaultValue) {
		return new MenuSetter<V>(defaultValue);
	}

	public void set(E value, Activity activity) {

		String previousValue = get().name();

		reference.set(value);

		toast(get().getDeclaringClass().getCanonicalName()
				+ " value was set from " + previousValue + " to "
				+ get().name(), activity);
	}

	public E get() {
		return reference.get();
	}
}
