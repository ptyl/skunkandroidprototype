package com.cloudon.libreoffice.android.enums;

public enum UnoUrlTarget {
	NEW("_blank"), DEFAULT("_default"), SELF("_self"), PARENT("_parent"), TOP(
			"_top"), SPECIAL_SUB_FRAME("_beamer");
	private final String target;

	private UnoUrlTarget(String target) {
		this.target = target;
	}

	public String getTarget() {
		return target;
	}
}