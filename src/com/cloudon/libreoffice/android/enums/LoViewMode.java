package com.cloudon.libreoffice.android.enums;

import com.cloudon.libreoffice.android.DesktopController;

public enum LoViewMode {
	ACTIVITY {
		@Override
		public void setViewImpl() {
			DesktopController.INSTANCE.setAsFullscreen();
		}
	},
	IN_LAYOUT {
		@Override
		public void setViewImpl() {
			DesktopController.INSTANCE.setAsWidget();
		}
	};

	public static final LoViewMode DEFAULT = IN_LAYOUT;

	protected abstract void setViewImpl();

	public void setView() {
		DesktopController.INSTANCE.createView();
		setViewImpl();
	}
}
