package com.cloudon.libreoffice.android.enums;

import org.libreoffice.experimental.desktop.R;

import android.annotation.SuppressLint;

import com.sun.star.uno.RuntimeException;

public enum WriterExtension {
	DOCX {
		@Override
		protected int getFile1RefId() {

			return R.raw.file1;
		}

		@Override
		protected int getFile2RefId() {

			return R.raw.file2;
		}
	},
	ODT {
		@Override
		protected int getFile1RefId() {

			return R.raw.filea;
		}

		@Override
		protected int getFile2RefId() {

			return R.raw.fileb;
		}
	};

	@SuppressLint("DefaultLocale")
	public String getExtension() {
		return name().toLowerCase();
	}

	public static final WriterExtension DEFAULT = ODT;

	protected abstract int getFile1RefId();

	protected abstract int getFile2RefId();

	public int getFileRefId(int index) {
		switch (index) {
		case 1:
			return getFile1RefId();
		case 2:
			return getFile2RefId();
		default:
			throw new RuntimeException("Cannot get file index " + index);

		}
	}

}