package com.cloudon.libreoffice.android.enums;

import org.libreoffice.android.AppSupport;
import org.libreoffice.experimental.desktop.Desktop;

import android.app.Activity;
import android.view.View;

import com.cloudon.libreoffice.android.utils.Utils;

public enum DesktopAction {
	SCROLL_UP {
		@Override
		protected void executeImpl() {

			scroll(0, SCROLL_STEP);

		}
	},
	SCROLL_DOWN {
		@Override
		protected void executeImpl() {

			scroll(0, -SCROLL_STEP);

		}
	},
	ZOOM_UP {
		@Override
		protected void executeImpl() {
			zoom(ZOOM_STEP);

		}
	},
	ZOOM_DOWN {
		@Override
		protected void executeImpl() {
			zoom(-ZOOM_STEP);

		}
	};

	protected static void scroll(int x, int y) {

		AppSupport.scroll(x, y);

		log("Called AppSupport.scroll(" + x + ", " + y + ");");
	}

	protected static void zoom(float delta) {
		AppSupport.zoom(1F + delta, ZOOM_X, ZOOM_Y);

		log("Called AppSupport.zoom(" + (1F + delta) + ", " + ZOOM_X + ", "
				+ ZOOM_Y + ");");
	}

	private static final int SCROLL_STEP = 1, ZOOM_X = 470, ZOOM_Y = 170;
	private static final float ZOOM_STEP = 0.05F;

	public void execute() {
		View view = Desktop.theView;

		if (view != null) {

			log("Performing " + this);

			executeImpl();

			view.invalidate();
		}
	}

	public void setButton(Activity activity, int buttonRefId) {
		Utils.setButton(activity, buttonRefId, new Runnable() {

			@Override
			public void run() {
				execute();

			}
		});
	}

	protected abstract void executeImpl();

	protected static void log(String... strings) {
		Utils.log(strings);
	}
}
