package com.cloudon.libreoffice.android;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.app.Activity;

import com.cloudon.libreoffice.android.utils.Glossary;
import com.cloudon.libreoffice.android.utils.Utils;

public class FilesManager extends Utils implements Glossary {

	private final Map<Integer, File> tempFiles = new ConcurrentHashMap<Integer, File>();

	private FilesManager() {

	}

	private int getCurrentFileRefId(Activity activity) {
		return activity.getIntent().getExtras()
				.getInt(SOURCE_FILE_REF_ID_INT_KEY);
	}

	public File getTempFile(Activity activity) {

		Integer srcResId = getCurrentFileRefId(activity);

		synchronized (tempFiles) {

			if (!tempFiles.containsKey(srcResId)) {
				tempFiles.put(srcResId, createTempCopy(srcResId, activity));
			}
		}

		return tempFiles.get(srcResId);
	}

	public static final FilesManager INSTANCE = new FilesManager();

}
