package com.cloudon.libreoffice.android;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.cloudon.libreoffice.android.utils.SyncGetter;
import com.cloudon.libreoffice.android.utils.Utils;
import com.cloudon.libreoffice.android.utils.XFile;
import com.sun.star.awt.XToolkitExperimental;
import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

public class UnoUtils extends Utils {

	private static final UnoUtils INSTANCE = new UnoUtils();

	private final Map<XFile, XFile> xFiles = new ConcurrentHashMap<XFile, XFile>();

	private static final String TOOLKIT_CLASS_PATH = "com.sun.star.awt.Toolkit",
			DESKTOP_CLASS_PATH = "com.sun.star.frame.Desktop";

	public static UnoUtils getInstance(DesktopController controller) {

		notNull(controller, "Must get Desktop controller "
				+ "to create UNO instance");

		return INSTANCE;
	}

	public final SyncGetter<XComponentContext> context = new SyncGetter<XComponentContext>(
			XComponentContext.class) {

		@Override
		protected XComponentContext create() throws Throwable {

			return Bootstrap.defaultBootstrap_InitialComponentContext();
		}
	};

	public final SyncGetter<XMultiComponentFactory> factory = new SyncGetter<XMultiComponentFactory>(
			XMultiComponentFactory.class) {

		@Override
		protected XMultiComponentFactory create() throws Throwable {

			return context.get().getServiceManager();
		}
	};

	public final SyncGetter<XDesktop> xDesktop = new SyncGetter<XDesktop>(
			XDesktop.class) {

		@Override
		protected XDesktop create() throws Throwable {

			return queryInterface(XDesktop.class, desktop.get());
		}
	};

	public XFile getXFile(File file) {
		XFile key = new XFile(file);
		if (!xFiles.containsKey(key)) {
			synchronized (xFiles) {

				if (!xFiles.containsKey(key)) {

					xFiles.put(key, key);
				}
			}
		}
		return xFiles.get(key);
	}

	public void remove(XFile xFile) {
		xFiles.remove(xFile);
	}

	private final SyncGetter<Object> desktop = new SyncGetter<Object>(
			Object.class) {

		@Override
		protected Object create() throws Throwable {

			return createWithContext(DESKTOP_CLASS_PATH);
		}
	}, toolkit = new SyncGetter<Object>(Object.class) {
		@Override
		protected Object create() throws Throwable {

			return createWithContext(TOOLKIT_CLASS_PATH);
		}
	};

	public final SyncGetter<XComponentLoader> loader = new SyncGetter<XComponentLoader>(
			XComponentLoader.class) {

		@Override
		protected XComponentLoader create() throws Throwable {

			return queryInterface(XComponentLoader.class, desktop.get());
		}
	};

	public final SyncGetter<XToolkitExperimental> xToolkit = new SyncGetter<XToolkitExperimental>(
			XToolkitExperimental.class) {

		@Override
		protected XToolkitExperimental create() throws Throwable {

			return queryInterface(XToolkitExperimental.class, toolkit.get());
		}
	};

	private UnoUtils() {

	}

	private Object createWithContext(String classPath) {
		try {

			Object object = factory.get().createInstanceWithContext(classPath,
					context.get());

			return notNull(object, "Failed to create instance for classpath \""
					+ classPath + "\"");

		} catch (Throwable throwable) {

			throw up("failed to create instance for class path " + classPath,
					throwable);
		}
	}

	public <T> T queryInterface(Class<T> clazz, Object context) {

		try {

			T value = UnoRuntime.queryInterface(clazz, context);

			return notNull(value,
					"Creation of object of class " + clazz.getCanonicalName()
							+ " from \"" + context + "\" failed");

		} catch (Throwable throwable) {
			throw up(
					"Failed to create object of type "
							+ clazz.getCanonicalName(), throwable);
		}

	}

	public void closeOthers(XFile xFile) {
		for (XFile file : xFiles.keySet().toArray(new XFile[xFiles.size()])) {

			if (!file.equals(xFile)) {

				file.close();
			}
		}

	}

}
