package com.cloudon.libreoffice.android;

import java.io.File;

import org.libreoffice.android.AppSupport;
import org.libreoffice.experimental.desktop.Desktop;
import org.libreoffice.experimental.desktop.Desktop.BitmapView;
import org.libreoffice.experimental.desktop.LoViewer;

import com.cloudon.libreoffice.android.enums.LoViewMode;
import com.cloudon.libreoffice.android.enums.WriterExtension;
import com.cloudon.libreoffice.android.utils.MenuSetter;
import com.cloudon.libreoffice.android.utils.Utils;
import com.cloudon.libreoffice.android.utils.XFile;

public class DesktopController extends Utils {

	private LoViewer viewer = null;

	private UnoUtils uno = UnoUtils.getInstance(this);
	private Boolean isRegisteredDesktopForScreenUpadtes = false;

	public final MenuSetter<WriterExtension> extension = MenuSetter
			.create(WriterExtension.DEFAULT);

	public final MenuSetter<LoViewMode> viewMode = MenuSetter
			.create(LoViewMode.DEFAULT);

	public File getFile() {
		return viewer.getFile();
	}

	public static final DesktopController INSTANCE = new DesktopController();

	public String getUrl() {
		return toUrl(getFile());
	}

	public XFile getXFile() {
		return uno().getXFile(getFile());
	}

	public void setContext(LoViewer loViewerToOpen) {

		viewer = loViewerToOpen;

		viewMode.get().setView();

		getXFile().open();

	}

	public UnoUtils uno() {

		return uno;
	}

	private DesktopController() {

	}

	public void setAsWidget() {

		viewer.addWidget();

		if (!isRegisteredDesktopForScreenUpadtes) {

			synchronized (uno) {
				if (!isRegisteredDesktopForScreenUpadtes) {

					AppSupport.registerForDamageCallback(Desktop.class);

					isRegisteredDesktopForScreenUpadtes = true;
				}
			}

		}
	}

	public void setAsFullscreen() {
		viewer.setContentView(Desktop.theView);

	}

	public void createView() {
		synchronized (Desktop.theView) {
			Desktop.theView = new BitmapView(viewer);
		}
	}

}
