package org.libreoffice.experimental.desktop;

import java.io.File;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cloudon.libreoffice.android.DesktopController;
import com.cloudon.libreoffice.android.FilesManager;
import com.cloudon.libreoffice.android.enums.DesktopAction;
import com.cloudon.libreoffice.android.utils.Glossary;
import com.cloudon.libreoffice.android.utils.Utils;
import com.cloudon.libreoffice.android.utils.XFile;

public class LoViewer extends Activity implements Glossary {

	private LinearLayout layout = null;
	private File file = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.loviewer);

		initSaveButton();

		initFileInfoButton();

		initScrollButtons();

		layout = (LinearLayout) findViewById(R.id.loViewLayout);

		file = getTempFile();

		setFileInfo();

	}

	private void initScrollButtons() {
		DesktopAction.SCROLL_UP.setButton(this, R.id.upButton);
		DesktopAction.SCROLL_DOWN.setButton(this, R.id.downButton);

	}

	public File getFile() {
		return file;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

		DesktopController.INSTANCE.setContext(this);
	}

	private void initSaveButton() {

		Utils.setButton(this, R.id.saveFileButton, new Runnable() {

			@Override
			public void run() {
				Utils.confirmOrStubDialog(LoViewer.this,
						"Are you sure you want to save changes to URL" + NL
								+ "\"" + DesktopController.INSTANCE.getUrl()
								+ "\"", new Runnable() {

							@Override
							public void run() {
								XFile xFile = getXFile();
								xFile.save();

								setFileInfo();
							}
						});

			}

		});

	}

	private XFile getXFile() {
		return DesktopController.INSTANCE.getXFile();
	}

	private void initFileInfoButton() {

		Utils.setButton(this, R.id.updateFileInfoButton, new Runnable() {

			@Override
			public void run() {
				setFileInfo();

			}
		});

	}

	@Override
	public void onBackPressed() {

		super.onBackPressed();

		layout.removeView(Desktop.theView);

	}

	private File getTempFile() {

		return FilesManager.INSTANCE.getTempFile(this);
	}

	private void setFileInfo() {
		((TextView) findViewById(R.id.textView1)).setText("File: "
				+ file.getName() + "\r\nLast modified: "
				+ new Date(file.lastModified()));
	}

	public void addWidget() {

		layout.addView(Desktop.theView);

	}
}
