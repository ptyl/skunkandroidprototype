package org.libreoffice.experimental.desktop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.cloudon.libreoffice.android.DesktopController;
import com.cloudon.libreoffice.android.enums.LoViewMode;
import com.cloudon.libreoffice.android.enums.WriterExtension;
import com.cloudon.libreoffice.android.utils.Glossary;
import com.cloudon.libreoffice.android.utils.Utils;

public class Main extends Activity implements Glossary {
	private static Desktop desktop = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		setButtton(R.id.button1, 1);
		setButtton(R.id.button2, 2);

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (desktop == null) {

			synchronized (DesktopController.INSTANCE) {

				Desktop instance = new Desktop();
				instance.create(Main.this);

				desktop = instance;

			}
		}
	}

	private void setButtton(int buttonRefId, final int fileIndex) {

		Utils.setButton(this, buttonRefId, new Runnable() {

			@Override
			public void run() {
				Intent intent = new Intent(Main.this, LoViewer.class);

				intent.putExtra(SOURCE_FILE_REF_ID_INT_KEY,
						DesktopController.INSTANCE.extension.get()
								.getFileRefId(fileIndex));

				startActivity(intent);

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.selectDocx:
			DesktopController.INSTANCE.extension
					.set(WriterExtension.DOCX, this);
			break;
		case R.id.selectODT:
			DesktopController.INSTANCE.extension.set(WriterExtension.ODT, this);
			break;
		case R.id.selectActivity:
			DesktopController.INSTANCE.viewMode.set(LoViewMode.ACTIVITY, this);
			break;
		case R.id.selectInLayout:
			DesktopController.INSTANCE.viewMode.set(LoViewMode.IN_LAYOUT, this);
			break;
		default:
			Utils.toast("Unkown menu option", this);
		}
		return super.onOptionsItemSelected(item);
	}
}
